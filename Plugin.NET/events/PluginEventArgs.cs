﻿using PluginNET.error;
using System;
using System.Reflection;

namespace PluginNET.events
{
    /// <summary>
    /// 插件事件参数
    /// </summary>
    [Serializable]
    public class PluginEventArgs : EventArgs
    {
        /// <summary>
        /// 插件文件完整名称
        /// </summary>
        public string FullName { get; internal set; }

        /// <summary>
        /// 插件实例
        /// </summary>
        public object Instance { get; internal set; }

        /// <summary>
        /// 程序集实例
        /// </summary>
        public Assembly Assembly { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public PluginErrorTypes ErrorType { get; internal set; }

        /// <summary>
        /// 插件事件的类型
        /// </summary>
        public PluginEventTypes EventType { get; internal set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public Exception Exception { get; internal set; }

        /// <summary>
        /// 加载的类结构的类型
        /// </summary>
        public Type ClassType { get; internal set; }

        /// <summary>
        /// 设置此值为true以阻止本次操作继续执行
        /// </summary>
        public bool Cancel { get; set; }

        public PluginEventArgs()
        {
            ErrorType = PluginErrorTypes.None;
        }
    }
}
