﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginLib2
{
    public class PluginClass2 : IInterface
    {
        public string Method1()
        {
            return "这是从第二个插件里面来的数据";
        }

        public string Method2(string from)
        {
            return "这是从第二个插件里面来的数据:" + from;
        }
    }
}
